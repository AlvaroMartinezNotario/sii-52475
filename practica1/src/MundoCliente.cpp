// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

extern int bot_alarm_state; //Variable externa declarada en tenis que usaremos aquí

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

	//Cliente no se comunica con el logger
	/*
	//Mando mensaje de final de partida
	sprintf(buffer,"Fin de la partida\n");
	if(write(fd_fifo,buffer,sizeof(buffer))<0)
	{
		perror("Fallo de escritura");
		exit(1);
	}
	//Cierro el descriptor de archivo del fifo
	close(fd_fifo);
	*/
    //Destruye memoria compartida, antes de destruirla, hago que la accion cambie a 2, se la pase al bot, para que el bot salga de while y se cierre solo
    p_datosmembot->fin=1;
    if(munmap(proyeccion_bot,sizeof(datosmembot)))
	{
		perror("Error al desproyectar memoria bot");
		exit(1);
	}
	close(fd_fifo_servidor_write);
	close(fd_fifo_servidor_read);
	unlink("/tmp/cliente_servidor_teclas");
	unlink("/tmp/cliente_servidor_posiciones");

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	


    p_datosmembot->esfera = esfera;
    p_datosmembot->raqueta1 = jugador2;
    p_datosmembot->raqueta2 = jugador1;
    char cad[220];
    int n=read(fd_fifo_servidor_read,cad,sizeof(cad));
    if(n>0) sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2, &esfera.radio); 
	if(n<0)//Error de lectura
	{
		perror("Error al leer del buffer");
		exit(1);
	}
//Cliente no mueve nada ni genera ninguna interaccion, tan solo dibuja    
/*
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=5+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=5+2*rand()/(float)RAND_MAX;
		puntos2++;
		esfera.Disminuye_radio(0.05); // Al marcar punto la esfera se hace más pequeña

		sprintf(buffer,"El jugador 2 marca un punto, lleva un total de %d puntos\n",puntos2);
        if(write(fd_fifo,buffer,sizeof(buffer))<0)
    	{
			perror("Fallo de escritura, recuerda iniciar el logger antes que tenis");
			exit(1);
		}	

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-5-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-5-2*rand()/(float)RAND_MAX;
		puntos1++;
		esfera.Disminuye_radio(0.05); // Al marcar punto la esfera se hace más pequeña

		sprintf(buffer,"El jugador 2 marca un punto, lleva un total de %d puntos\n",puntos1);
        if(write(fd_fifo,buffer,sizeof(buffer))<0)
		{
			perror("Fallo de escritura, recuerda iniciar el logger antes que tenis");
			exit(1);
		}

	}
*/
    switch(p_datosmembot->accion)
    {
            case -1: OnKeyboardDown('o',0,0);break;
            case 0: break;
            case 1 : OnKeyboardDown('l',0,0);break;
    }
    /*
    if(bot_alarm_state==0) //Movimiento jugador1 por bot 
    {
            switch(p_datosmembot->accion2)
            {
                    case -1: OnKeyboardDown('w',0,0);break;
                    case 0: break;
                    case 1 : OnKeyboardDown('s',0,0);;break;
            }
    }
*/
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char key_pressed[100];
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
    case 's': sprintf(key_pressed, "s");
    /* bot_alarm_state=1; // Así activamos una alarma de 5s y mientras tanto el estado de la alarma en 1 no enciende el bot de jugador 1
     alarm(5); */
    break;
    case 'w':sprintf(key_pressed, "w");
    /*bot_alarm_state=1;
     alarm(5);*/
    break;
    case 'l':sprintf(key_pressed, "l");
    break;
    case 'o':sprintf(key_pressed, "o");
    break;
	}
	if(write(fd_fifo_servidor_write,key_pressed,sizeof(key_pressed))<0){
		perror("Error al escribir valor de tecla");
		exit(1);
	}
}

void CMundo::Init()
{
/*
    bot_alarm_state=1;
    alarm(5);
*/

/*
//Abrimos el fifo en modo escritura
	if((fd_fifo=open("/tmp/fifologger", O_WRONLY))<0)
	{
		perror("No se puede abrir el FIFO, es necesario ejecutar logger primero");
		exit(1);
	}
*/
    //Abrimos fifos para comunicarnos con el servidor
	if(mkfifo("/tmp/cliente_servidor_teclas",0777)<0) // Creo el FIFO
	{
		perror("Error al crear el FIFO teclas");
		exit(1);
	} 
	 // Abro el FIFO y guardo su file descriptor
	if((fd_fifo_servidor_write=open("/tmp/cliente_servidor_teclas",O_WRONLY))<0) /// Error al abrir el fifo
	{
		perror("Error al abir el fifo teclas");
		exit(1);
	}


	if(mkfifo("/tmp/cliente_servidor_posiciones",0777)<0) // Creo el FIFO
	{
		perror("Error al crear el FIFO posiciones");
		exit(1);
	} 
	 // Abro el FIFO y guardo su file descriptor
	if((fd_fifo_servidor_read=open("/tmp/cliente_servidor_posiciones",O_RDONLY))<0) /// Error al abrir el fifo
	{
		perror("Error al abrir el fifo lectura posiciones");
		exit(1);
	}
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

    int fdbot=open("/tmp/botdatos.txt",O_CREAT|O_TRUNC | O_RDWR, 0777);
    if(fdbot<0)
    {
    	perror("Error al crear el fichero botdatos");
    	exit(1);
    }
    //Se cogen los datos del DatosMemCompartida, por tanto en el & no se puede poner el puntero p_datosmembot)
    if (ftruncate(fdbot, sizeof(datosmembot)))
        {
                perror("Error en ftruncate del fichero datos bot");
                exit(1);
        }
    //Proyecto el fichero en memoria
    proyeccion_bot=(char *)mmap(NULL,sizeof(datosmembot),PROT_WRITE|PROT_READ,MAP_SHARED,fdbot,0);
    if(proyeccion_bot == MAP_FAILED)
	{
		perror("Error al proyectar memoria bot");
		exit(1);
	}
    //Una vez proyectado el fichero, este ya no es necesario
    close(fdbot);
    //Convierto la proyeccion, de tipo void * a DatosMemCompartida * y los guardo en el puntero p_datosmembot
    p_datosmembot=(DatosMemCompartida*)proyeccion_bot;
    p_datosmembot->accion=0; //Accion inicial a 0
    p_datosmembot->accion2=0;

}
