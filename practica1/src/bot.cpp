#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include <error.h>

int main() 
{
	int err_munmap;
	int fd;
	char *proyeccion;
	DatosMemCompartida *p_mem;
	int estado=1;
	
	//Comprobamos que no hay errores y a la vez abrimos el fichero ya creado por mundo en caso de no haber error.
	if((fd=open("/tmp/botdatos.txt",O_RDWR))<0)
	{
		perror("Error al abrir fichero botdatos");
		return -1;
	}
	if (ftruncate(fd, sizeof(*p_mem)))
        {
                perror("Error en ftruncate del fichero");
                close(fd);
                return(1);
        }
	//Proyectamos fichero, al ser el mismo identificador de fichero, se compartira memoria entre los 2 procesos
	proyeccion=(char *)mmap(NULL,sizeof(*(p_mem)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);
	if(proyeccion == MAP_FAILED)
	{
		perror("Error al proyectar memoria bot");
		exit(1);
	}
	//Cerramos fichero, no es necesario tener abierto el fd
	close(fd);
	//Le damos la direccion de memoria al puntero
	p_mem=(DatosMemCompartida*)proyeccion;
	//Codigo del bot para mover la raqueta
	
	while(estado)
	{
		//25 ms de bloqueo
		usleep(25000);
	//Para raqueta1. Siempre usada por bot
		float centro_raqueta1=((p_mem->raqueta1.y1+p_mem->raqueta1.y2)/2);
		if(centro_raqueta1<p_mem->esfera.centro.y) p_mem->accion=-1;
		else if(centro_raqueta1>p_mem->esfera.centro.y) p_mem->accion=1;
		else p_mem->accion=0;
		/*
	//RAQUETA 2 UNA VEZ EL HANDLER SE ACTIVA
		float centro_raqueta2= ((p_mem->raqueta2.y1+p_mem->raqueta2.y2)/2);
		if(centro_raqueta2<p_mem->esfera.centro.y) p_mem->accion2=-1;
		else if(centro_raqueta2>p_mem->esfera.centro.y) p_mem->accion2=1;
		else p_mem->accion2=0;
*/
		if(p_mem->fin == 1) estado=0; // Si cerramos tenis fin se pone a 1 y se cierra bot
	}
	err_munmap = munmap(proyeccion,sizeof(*p_mem));
	if(err_munmap<0)
	{
		perror("Error al desproyectar memoria bot");
		exit(1);
	}
	printf("Cerrando bot...\n");
	return 0;
}

