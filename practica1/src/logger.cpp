#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


#define MAX_BUFF 70

int main(int argc, char* argv[])
{
	if(mkfifo("/tmp/fifologger",0777)<0) // Creo el FIFO
	{
		perror("Error al crear el FIFO");
		exit(1);
	} 
	int fd=open("/tmp/fifologger",O_RDONLY); // Abro el FIFO y guardo su file descriptor
	if(fd<1) /// Error al abrir el fifo
	{
		perror("Error al abir el fifo");
		exit(1);
	}
	while(1) { //Bucle infinito de lectura
		char buffer[MAX_BUFF]; // Creamos el Buffer. El buffer lo meto aquí para que en cada ciclo de lectura se resetee.
		int n = read(fd,buffer,sizeof(buffer)); // Numero de bits leidos
		if(n>0) printf("%s\n", buffer);
		//Condicion fin del bucle
		if(n==0) break; // Read al estar leyendo un FIFO se bloquea si el buffer está vacio
		// Y ese read devuelve cero si se ha cerrado el escritor del Buffer en el FIFO
		if(n<0)//Error de lectura
		{
			perror("Error al leer del buffer");
			exit(1);
		}
	}
	// Cierre y borrado del FIFO
	printf("Cerrando el logger...\n");
	close(fd);
	unlink("/tmp/fifologger");
	return 0;
}